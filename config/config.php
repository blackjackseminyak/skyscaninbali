<?php

return [
    /*
    |-----------------------------------------------------------------------------
    | SkyScanner API credentials (http://partners.api.skyscanner.net/apiservices/)
    |-----------------------------------------------------------------------------
    |
    */
    'api_key' => 'prtl6749387986743898559646983194',
    /*
    |-----------------------------------------------------------------------------
    | Header Values The default response format is XML (application/xml)
    |-----------------------------------------------------------------------------
    |
    */
    'Accept' => 'application/json',
    /*
    |-----------------------------------------------------------------------------
    | The default Skyscanner Travel APIs manager class
    |-----------------------------------------------------------------------------
    |
    */
    
    'skyscan_manager_class' => @'SkyScaninBali\SkyScan\SkyScanManager',
    /*
    |-----------------------------------------------------------------------------
    | The Browse Qoutes API that retrieve the cheapest quotes from our cache prices.
    |-----------------------------------------------------------------------------
    |
    */
    'browse_quotes' => 'browsequotes',
    /*
    |-----------------------------------------------------------------------------
    | The Skyscanner Travel APIs Retrieve the cheapest routes from our cache prices. 
    |-----------------------------------------------------------------------------
    |
    */
    'browse_routes' => 'browseroutes',
    /*
    |-----------------------------------------------------------------------------
    | The Browse Qoutes API that retrieve the cheapest quotes from our cache prices.
    |-----------------------------------------------------------------------------
    |
    */
    'browse_quotes' => 'browsequotes',
    /*
    |-----------------------------------------------------------------------------
    | The Skyscanner Travel APIs Retrieve the cheapest routes from our cache prices. 
    |-----------------------------------------------------------------------------
    |
    */
    'browse_routes' => 'browseroutes',
    /*
    |-----------------------------------------------------------------------------
    | The Browse Dates API that Retrieve the cheapest dates for a given route from our cache.
    |-----------------------------------------------------------------------------
    |
    */
    'browse_dates' => 'browsedates',
    /*
    |-----------------------------------------------------------------------------
    | The Browse Dates (grid) API that retrieve the cheapest dates for a given route 
    | from our cache, with the results formatted as a two-dimensional array to be 
    | easily displayed as a calendar.
    |-----------------------------------------------------------------------------
    |
    */
    
    'browse_grid' => 'browsegrid',
    
];