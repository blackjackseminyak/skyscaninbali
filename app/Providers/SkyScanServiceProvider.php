<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class SkyScanServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if (method_exists($this, 'publishes')) {
            $this->loadViewsFrom(__DIR__.'/../../views', 'skyscaninbali');
            $this->publishes([
                __DIR__.'/../../config/config.php' => config_path('skyscaninbali.php'),
                __DIR__.'/../../views' => base_path('resources/views/vendor/skyscaninbali'),
            ]);
        } else {
            $this->package('skyscan/skyscaninbali');
        }
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        if (method_exists($this, 'publishes')) {
            $this->registerResources();
        }
        $this->app->singleton('feed_manager', function ($app) {
            $manager_class = $app['config']->get('skyscaninbali::feed_manager_class');
            $api_key = $app['config']->get('skyscaninbali::api_key');
            return new $manager_class($this->app['config'], $api_key);
        });
    }
    
     /**
     * Register the package resources.
     *
     * @return void
     */
    protected function registerResources()
    {
        $userConfigFile = $this->app->configPath().'/skyscaninbali.php';
        $packageConfigFile = __DIR__.'/../../config/config.php';
        $config = $this->app['files']->getRequire($packageConfigFile);
        if (file_exists($userConfigFile)) {
            $userConfig = $this->app['files']->getRequire($userConfigFile);
            $config = array_replace_recursive($config, $userConfig);
        }
        $namespace = 'skyscaninbali::';
        foreach($config as $key => $value) {
            $this->app['config']->set($namespace . $key , $value);
        }
    }
}
