<?php
namespace App;
use Exception;
use App;
class SkyScanManager
{
    /**
     * @var Client
     */
    public $client;
    /**
     * @var mixed
     */
    private $config;
    /**
     * @var string
     */
    private $userFeed;
    /**
     * @param string $api_key
     * @param mixed $config
     *
     * @throws Exception
     */
    public function __construct($api_key, $api_secret, $config)
    {
        $this->config = $config;
        if (getenv('SKYSCANNERAPI_URL') !== false) {
            $this->client = Client::herokuConnect(getenv('SKYSCANNERAPI_URL'));
        } else {
            $this->client = new Client($api_key, $api_secret);
            $market_country = $this->config->get("skyscaninbali::market_country");
            //$this->client->setLocation($location);
            $this->client->timeout = $this->config->get("skyscaninbali::timeout", 3);
        }
        $this->browse_quotes = $this->config->get("skyscaninbali::quotes");
    }
    /**
     * @param string country
     * @param string currency
     * @param string locale
     * @param string originPlace 
     * @param string destinationPlace
     * @param string outboundPartialDate
     * @param string inboundPartialDate
     * @param string apiKey 
     * 
     * @return \apiservices\Qoutes
     */
    public function getBrowseQuotes($country, $currency, $locale, $originPlace, $destinationPlace, $outboundPartialDate, $inboundPartialDate, $apiKey)
    {
        $quotes = [];
        $browse_quotes = $this->config->get("skyscaninbali::browse_quotes");
        foreach ($browse_quotes as $quote) {
            $quotes[$quote] = $this->client->feed($quote, $apiKey);
        }
        return $quotes;
    }
    /**
     * @param string country
     * @param string currency
     * @param string locale
     * @param string originPlace 
     * @param string destinationPlace
     * @param string outboundPartialDate
     * @param string inboundPartialDate
     * @param string apiKey 
     * 
     * @return \apiservices\Routes
     */
    public function getBrowseRoutes($country, $currency, $locale, $originPlace, $destinationPlace, $outboundPartialDate, $inboundPartialDate, $apiKey)
    {
        $routes = [];
        $browse_routes = $this->config->get("skyscaninbali::browse_routes");
        foreach ($browse_routes as $route) {
            $quotes[$route] = $this->client->feed($route, $apiKey);
        }
        return $routes;
    }
    /**
     * @param string country
     * @param string currency
     * @param string locale
     * @param string originPlace 
     * @param string destinationPlace
     * @param string outboundPartialDate
     * @param string inboundPartialDate
     * @param string apiKey 
     * 
     * @return \apiservices\Dates
     */
    public function getBrowseDates($country, $currency, $locale, $originPlace, $destinationPlace, $outboundPartialDate, $inboundPartialDate, $apiKey)
    {
        $dates = [];
        $browse_dates = $this->config->get("skyscaninbali::browse_dates");
        foreach ($browse_dates as $date) {
            $quotes[$date] = $this->client->feed($date, $apiKey);
        }
        return $dates;
    }
    /**
     * @param string country
     * @param string currency
     * @param string locale
     * @param string originPlace 
     * @param string destinationPlace
     * @param string outboundPartialDate
     * @param string inboundPartialDate
     * @param string apiKey 
     * 
     * @return \apiservices\Dates
     */
    public function getBrowseGrid($country, $currency, $locale, $originPlace, $destinationPlace, $outboundPartialDate, $inboundPartialDate, $apiKey)
    {
        $dates = [];
        $browse_dates = $this->config->get("skyscaninbali::browse_grid");
        foreach ($browse_dates as $date) {
            $quotes[$date] = $this->client->feed($date, $apiKey);
        }
        return $dates;
    }
    public function getClient()
    {
        return $this->client;
    }
    public function getFeed($feed, $user_id)
    {
        return $this->client->feed($feed, $apiKey);
    }
}