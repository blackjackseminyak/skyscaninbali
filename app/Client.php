<?php
namespace App\SkyScan;
use Exception;
const VERSION = 'v1.0';
class Client
{
    const API_ENDPOINT = 'partners.api.skyscanner.net/apiservices';
    /**
     * @var string
     */
    protected $api_key;
    /**
     * @var string
     */
    protected $location;
    /**
     * @var string
     */
    protected $protocol;
    /**
     * @var string
     */
    public $api_version;
    /**
     * @var float
     */
    public $timeout;
    /**
     * @param string $api_key
     * @param string $api_secret
     * @param string $api_version
     * @param string $location
     * @param float $timeout
     */
    public function __construct($api_key, $api_version='v1.0', $location='', $timeout=3.0)
    {
        $this->api_key = $api_key;
        $this->api_version = $api_version;
        $this->timeout = $timeout;
        $this->location = $location;
        $this->protocol = 'http';
    }
    /**
     * @param  string|null $url
     * @return Client
     * @throws Exception
     */
    public static function herokuConnect($url = null)
    {
        if ($url === null) {
            $url = getenv('SKYSCANAPI_URL');
        }
        $parsed_url = parse_url($url);
        $api_key = $parsed_url['apiKey'];
        if ($api_key == '') {
            throw new Exception('url malformed');
        }
        $client = new static($api_key);
        return $client;
    }
    /**
     * @param  string $protocol
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;
    }
    /**
     * @param  string $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }
    /**
     * @param  string $feed_slug
     * @param  string $user_id
     * @param  string|null $token
     * @return Feed
     */
    public function feed($feed_slug, $user_id, $token = null)
    {
        if (null === $token) {
            $token = $this->signer->signature($feed_slug . $user_id);
        }
        return new Feed($this, $feed_slug, $user_id, $this->api_key, $token);
    }
    /**
     * @return string
     */
    public function getBaseUrl()
    {
        $baseUrl = getenv('SKYSCAN_BASE_URL');
        if (!$baseUrl) {
            $api_endpoint = static::API_ENDPOINT;
            $localPort = getenv('SKYSCAN_LOCAL_API_PORT');
            if ($localPort) {
                $baseUrl = "http://localhost:$localPort/api";
            } else {
                if ($this->location) {
                    $subdomain = "{$this->location}-api";
                } else {
                    $subdomain = 'api';
                }
                $baseUrl = "{$this->protocol}://{$subdomain}." . $api_endpoint;
            }
        }
        return $baseUrl;
    }
    /**
     * @param  string $uri
     * @return string
     */
    public function buildRequestUrl($uri)
    {
        $baseUrl = $this->getBaseUrl();
        return "{$baseUrl}/{$this->api_version}/{$uri}";
    }
}